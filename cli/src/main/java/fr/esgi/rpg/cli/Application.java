package fr.esgi.rpg.cli;

public class Application {
    public static void main (String args[]){

        Tournament tournament = new Tournament();
        Tournament.interactiveBuildTournament(tournament);

        System.out.println(tournament.toString());

    }
}
