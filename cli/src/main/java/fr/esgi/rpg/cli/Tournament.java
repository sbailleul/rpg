package fr.esgi.rpg.cli;

import fr.esgi.rpg.core.*;

import java.lang.Character;
import java.util.ArrayList;

public class Tournament {

    ArrayList<Entity> population;
    ArrayList<Faction> factions;

    public Tournament(ArrayList<Entity> population, ArrayList<Faction> factions) {
        this.population = population;
        this.factions = factions;
    }

    public Tournament() {
        this.population = new ArrayList<>();
        this.factions = new ArrayList<>();
    }

    public ArrayList<Entity> getPopulation() {
        return population;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("Tournament{population=");
        for (Entity people: this.population ){
            result.append(people.toString());
        }
        result.append(", factions = " );
        for (Faction faction: this.factions) {
            result.append(faction.toString());
        }
        return result.toString();
    }

    public void setPopulation(ArrayList<Entity> population) {
        this.population = population;
    }

    public ArrayList<Faction> getFactions() {
        return factions;
    }

    public void setFactions(ArrayList<Faction> factions) {
        this.factions = factions;
    }


    public static void interactiveBuildTournament(Tournament tournament) {
        System.out.println("lancer le tournoy du roy");
        System.out.println("on créé un perso guerrier");
        Warrior kadok = new Warrior("Kadok");
        tournament.population.add(kadok);
        System.out.println("kadok rejoint les semi croustillants");
        Faction halfCrunchy = new Faction("semi croustillants");
        tournament.factions.add(halfCrunchy);
        kadok.joinFaction(halfCrunchy);

        System.out.println("les semi croustillants sont maintenant amis avec kaamelot");

        Faction kaamelott = new Faction("Kaamelott");
        tournament.factions.add(kaamelott);
        halfCrunchy.addFriend(kaamelott);

        System.out.println("on créé un perso mage");
        Mage merlin = new Mage("Merlin");
        merlin.joinFaction(kaamelott);
        tournament.population.add(merlin);


        System.out.println("kadok se frappe seul");
        kadok.attemptToAttack(kadok);

        System.out.println("merlin soigne kadok");
        merlin.attemptToHeal(kadok);


        System.out.println("les factions ne sont plus amies");
        halfCrunchy.removeFriend(kaamelott);

        System.out.println("kadok frappe merlin");
        kadok.attemptToAttack(merlin);

        System.out.println("merlin attaque kadok... mais rien ne se passe");
        merlin.attemptToHeal(kadok);

    }
}
