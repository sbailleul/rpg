package fr.esgi.rpg.core;

public class Mage extends Character {

    public Mage(String name) {
        super(name);
        this.attack = new KindAttack();
        this.heal = new SuperHeal();
    }
}
