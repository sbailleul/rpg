package fr.esgi.rpg.core;

public class DefaultAttack implements Attack {

    @Override
    public boolean attack(Character characterToAttack) {

        if(characterToAttack.isAlive()){
            characterToAttack.alterHealth(-1);
        }
        return characterToAttack.isAlive();
    }

    @Override
    public boolean canAttack(Character attackingCharacter, Character characterToAttack) {
        if (attackingCharacter.isAlly(characterToAttack) && !attackingCharacter.isSameCharacter(characterToAttack)) {
            return false;
        }
        return true;
    }
}
