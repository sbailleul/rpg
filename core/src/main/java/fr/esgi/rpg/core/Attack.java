package fr.esgi.rpg.core;

public interface Attack {

    public boolean attack(Character characterToAttack);

    public boolean canAttack(Character attackingCharacter, Character characterToAttack);
}
