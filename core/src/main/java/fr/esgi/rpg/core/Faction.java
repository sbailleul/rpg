package fr.esgi.rpg.core;

import java.util.ArrayList;

public class Faction {

    private String name;
    private ArrayList<Faction> friendFactions;

    public Faction(String name) {
        this.name = name;
        this.friendFactions = new ArrayList<Faction>();
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }


    public ArrayList<Faction> getFriendFactions() {
        return friendFactions;
    }

    public void setFriendFactions(ArrayList<Faction> friendFactions) {
        this.friendFactions = friendFactions;
    }

    public int addFriend(Faction friend){
        this.friendFactions.add(friend);
        return this.friendFactions.indexOf(friend);
    }

    public int removeFriend(Faction friend){
        int indexOfFriend = this.friendFactions.indexOf(friend);
        this.friendFactions.remove(friend);
        return indexOfFriend;
    }

    public boolean isFriend(Faction friend){
        return this.friendFactions.contains(friend);
    }

    public boolean isInMyFriendList(ArrayList<Faction> friendListToTest){

        for (Faction factToTest: friendListToTest) {
            if(this.isFriend(factToTest)){
                return true;
            }
        }
        return false;
    }


    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "Faction : " + this.name;
    }
}
