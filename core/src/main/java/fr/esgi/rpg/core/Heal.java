package fr.esgi.rpg.core;

public interface Heal {

    public boolean heal(Character characterToHeal);

    public boolean canHeal(Character healer, Character characterToHeal);


}
