package fr.esgi.rpg.core;

import java.util.Random;

public class SuperHeal implements Heal {
    @Override
    public boolean heal(Character characterToHeal) {

        int randHeal = new Random().nextInt(5) + 5;
        characterToHeal.alterHealth(randHeal);

        return characterToHeal.isAlive();
    }

    @Override
    public boolean canHeal(Character healer, Character characterToHeal) {
        return healer.isAlly(characterToHeal);
    }
}


