package fr.esgi.rpg.core;

public class EgoHeal implements Heal{

    @Override
    public boolean heal(Character characterToHeal) {
        characterToHeal.alterHealth(1);
        return characterToHeal.isAlive();
    }

    @Override
    public boolean canHeal(Character healer, Character characterToHeal) {
        return healer.isSameCharacter(characterToHeal);
    }
}
