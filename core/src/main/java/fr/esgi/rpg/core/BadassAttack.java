package fr.esgi.rpg.core;

import java.util.Random;

public class BadassAttack implements Attack {

    @Override
    public boolean attack(Character characterToAttack) {

        int randDamage = new Random().nextInt(10);

        if(characterToAttack.isAlive()){
            characterToAttack.alterHealth(-randDamage);
        }
        return characterToAttack.isAlive();
    }

    @Override
    public boolean canAttack(Character attackingCharacter, Character characterToAttack) {

        if(!characterToAttack.equals(attackingCharacter) && attackingCharacter.isAlly(characterToAttack)){
            return false;
        }
        return true;
    }
}
