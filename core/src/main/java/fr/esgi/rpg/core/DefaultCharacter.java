package fr.esgi.rpg.core;

public class DefaultCharacter extends Character {

    public DefaultCharacter(String name) {
        super(name);
        this.attack = new DefaultAttack();
        this.heal = new DefaultHeal();
    }


}
