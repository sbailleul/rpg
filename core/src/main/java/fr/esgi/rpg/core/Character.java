package fr.esgi.rpg.core;

import java.util.ArrayList;

public abstract class Character extends Entity{

    protected Attack attack;
    protected Heal heal;
    protected String name;
    protected ArrayList<Faction> factions;

    public Character(String name) {
        this.health = 100;
        this.name = name;
        this.factions = new ArrayList<Faction>();
    }

    public Attack getAttack() {
        return attack;
    }

    public void setAttack(Attack attack) {
        this.attack = attack;
    }

    public Heal getHeal() {
        return heal;
    }

    public void setHeal(Heal heal) {
        this.heal = heal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public ArrayList<Faction> getFactions() {
        return factions;
    }

    public void setFactions(ArrayList<Faction> factions) {
        this.factions = factions;
    }

    public void alterHealth(int healthToAlter){

        if(this.health + healthToAlter < 0){
            this.health = 0;
        } else if(this.health + healthToAlter > 100) {
            this.health = 100;
        } else {
            this.health += healthToAlter;
        }
    }

    public boolean isAlive(){
        return this.health != 0;
    }

    public boolean isSameCharacter(Character otherCharacter){
        return this.equals(otherCharacter);
    }

    public boolean isAlly(Character characterToTest){

        for (Faction myFac: this.factions) {
            if(myFac.isInMyFriendList(characterToTest.factions) || characterToTest.factions.contains(myFac)){
                return true;
            }
        }
        return false;
    }

    public void joinFaction(Faction factionToJoin){

        if(!this.factions.contains(factionToJoin)){
            this.factions.add(factionToJoin);
        }
    }

    public void leaveFaction(Faction factionToLeave){
        this.factions.remove(factionToLeave);
    }

    public boolean attemptToAttack(Character characterToAttack){
        if(this.attack.canAttack(this,characterToAttack)){
            return this.attack.attack(characterToAttack);
        }
        return false;
    }

    public boolean attemptToHeal(Character characterToHeal){
        if(this.heal.canHeal(this, characterToHeal)){
            return this.heal.heal(characterToHeal);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "Character, name : " + this.name + ", job : " + this.getClass().getSimpleName() + " pv : " + this.health;
    }
}
